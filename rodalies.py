from app.libraries.setup import Setup
from app.libraries.api import DO
from app.libraries.mentions import Mentions
from mastodon import Mastodon, StreamListener
import re
import time
import sys
import os
import requests
from io import BytesIO
import feedparser
import emojis
from datetime import datetime
import pdb

def remove_tz(published):
    print(time.localtime().tm_isdst)
    return published.replace(" +0100","") if time.localtime().tm_isdst == 0 else published.replace(" +0200","")

class Listener(StreamListener):

    def on_notification(self, notification):

        if notification.type != 'mention':

            return

        else:

            qry = Mentions(mastodon)

            reply, query_word, username, status_id, visibility = qry.get_data(notification)

            if reply == True:

                try:

                    resp = requests.get(f'http://www.gencat.cat/rodalies/incidencies_rodalies_rss_{query_word}_ca_ES.xml', timeout=5)

                except requests.exceptions.ConnectTimeout as conntimeout:

                    pass

                content = BytesIO(resp.content)

                feed = feedparser.parse(content)

                title = feed.feed.title

                status = feed.entries[0].title

                published = feed.feed.published

                published = datetime.strptime(remove_tz(published), '%a, %d %b %Y %H:%M:%S')

                published = published.strftime('%d %b %Y %H:%M:%S')

                if 'Normalitat al servei' in status:

                    toot_text = f'@{username}\n\n{title}\n\n{emojis.encode(":white_check_mark:")} {status}\n\nactualitzat: {published}'

                else:

                    toot_text = f'@{username}\n\n{title}\n\n{emojis.encode(":x:")} {status}\n\nactualitzat: {published}'

                print(f'Tooting...\n"{toot_text}')

                mastodon.status_post(toot_text, in_reply_to_id=status_id, visibility=visibility)

if __name__ == '__main__':

    setup = Setup()

    mastodon = Mastodon(
            access_token = setup.mastodon_app_token,
            api_base_url= setup.mastodon_hostname
            )

    mastodon.stream_user(Listener())
