import os
import sys
import random
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import pdb

class Database():

    name = 'database library'

    def __init__(self, config_file=None, db=None, db_user=None, db_user_password=None):

        self.config_file = "config/db_config.txt"
        self.db = self.__get_parameter("db", self.config_file)
        self.db_user = self.__get_parameter("db_user", self.config_file)
        self.db_user_password = self.__get_parameter("db_user_password", self.config_file)

        db_setup = self.__check_dbsetup(self)

        if not db_setup:

            self.db = input("\ndatabase name: ")
            self.db_user = input("\ndatabase user: ")
            self.db_user_password = input("\ndatabase user password: ")

            self.__createdb(self)  
            self.__create_config(self)
            self.__write_config(self)

    def read(self):

        lines = []

        select_sql = "select checked_at, line, status, issue from rodalies order by checked_at desc limit 12"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(select_sql)

            rows = cur.fetchall()

            for row in rows:

                lines.append(row)

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        return lines

    def save(self, date, linia, status, issue):

        insert_sql = "INSERT INTO rodalies(checked_at, line, status, issue) VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (date, linia, status, issue))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    @staticmethod
    def __check_dbsetup(self):

        db_setup = False

        try:

            conn = None

            conn = psycopg2.connect(database = self.db, user = self.db_user, password = self.db_user_password, host = "/var/run/postgresql", port = "5432")

            db_setup = True

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        return db_setup

    @staticmethod
    def __createdb(self):

        conn = None

        try:

            conn = psycopg2.connect(dbname='postgres',
                user=self.db_user, host='',
                password=self.db_user_password)

            conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

            cur = conn.cursor()

            print(f"Creating database {self.db}. Please wait...")

            cur.execute(sql.SQL("CREATE DATABASE {}").format(
                sql.Identifier(self.db))
            )
            print(f"Database {self.db} created!\n")

            self.__dbtables_schemes(self)

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    @staticmethod
    def __dbtables_schemes(self):

        table = "rodalies"
        sql = f"create table {table} (checked_at timestamptz, line varchar(3), status boolean default true, issue varchar(200))"
        self.__create_table(self, table, sql)

        index = "rodalies_pkey"
        sql_index = f'CREATE UNIQUE INDEX {index} ON {table} (checked_at, line)'
        self.__create_index(self, table, index, sql_index)

    @staticmethod
    def __create_table(self, table, sql):

        conn = None

        try:

            conn = psycopg2.connect(database = self.db, user = self.db_user, password = self.db_user_password, host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            print(f"Creating table {table}")

            cur.execute(sql)

            conn.commit()

            print(f"Table {table} created!\n")

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    @staticmethod
    def __create_index(self, table, index, sql_index):

      conn = None
      try:

        conn = psycopg2.connect(database = self.db, user = self.db_user, password = "", host = "/var/run/postgresql", port = "5432")
        cur = conn.cursor()

        print(f"Creating index...{index}")
        # Create the table in PostgreSQL database
        cur.execute(sql_index)

        conn.commit()

        print(f"Index {index} created!\n")

      except (Exception, psycopg2.DatabaseError) as error:

        print(error)

      finally:

        if conn is not None:

          conn.close()

    def __get_parameter(self, parameter, config_file):

        if not os.path.isfile(config_file):
            print(f"File {config_file} not found..")
            return

        with open( config_file ) as f:
            for line in f:
                if line.startswith( parameter ):
                    return line.replace(parameter + ":", "").strip()

        print(f"{config_file} Missing parameter {parameter}")

        sys.exit(0)

    @staticmethod
    def __create_config(self):

        if not os.path.exists('config'):

            os.makedirs('config')

        if not os.path.exists(self.config_file):

            print(self.config_file + " created!")
            with open(self.config_file, 'w'): pass

    @staticmethod
    def __write_config(self):

        with open(self.config_file, 'a') as the_file:

            the_file.write(f'db: {self.db}\ndb_user: {self.db_user}\ndb_user_password: {self.db_user_password}\n')

            print(f"adding parameters to {self.config_file}\n")


