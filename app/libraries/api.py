import os
import requests

class DO:

    name = "Dades Obertes Generalitat de Catalunya"

    def __init__(self, api_base_url=None, client_id=None, client_secret=None, session=None, headers=None):

        self.__dadesobertes_config_path = "config/dadesobertes.txt"

        is_setup = self.__check_setup(self)

        if is_setup:

            self.api_base_url = self.__get_parameter("api_base_url", self.__dadesobertes_config_path)
            self.client_id = self.__get_parameter("client_id", self.__dadesobertes_config_path)
            self.client_secret = self.__get_parameter("client_secret", self.__dadesobertes_config_path)

        else:

            self.api_base_url, self.client_id, self.client_secret = self.__setup(self)

        self.headers = {"Authorization": "Bearer " + self.client_id} if self.client_id else {}

        if session:
            self.session = session
        else:
            self.session = requests.Session()

    @staticmethod
    def __check_setup(self):

        is_setup = False

        if not os.path.isfile(self.__dadesobertes_config_path):
            print(f"File {self.__dadesobertes_config_path} not found, running setup.")
        else:
            is_setup = True

        return is_setup

    @staticmethod
    def __setup(self):

        if not os.path.exists('config'):
            os.makedirs('config')

        self.api_base_url = input("Dades Obertes API url, per exemple. 'https://des.gestorapi.gencat.cat': ")
        self.client_id = input("Dades obertes client id: ")
        self.client_secret = input("Dades obertes client secret: ")

        if not os.path.exists(self.__dadesobertes_config_path):
            with open(self.__dadesobertes_config_path, 'w'): pass
            print(f"{self.__dadesobertes_config_path} created!")

        with open(self.__dadesobertes_config_path, 'a') as the_file:
            print("Writing dadesobertes parameters to " + self.__dadesobertes_config_path)
            the_file.write(f'api_base_url: {self.api_base_url}\n'+f'client_id: {self.client_id}\n'+f'client_secret: {self.client_secret}')

        return (self.api_base_url, self.client_id, self.client_secret)

    @staticmethod
    def __get_parameter(parameter, file_path ):

        with open( file_path ) as f:
            for line in f:
                if line.startswith( parameter ):
                    return line.replace(parameter + ":", "").strip()

        print(f'{file_path} Missing parameter {parameter}')
        sys.exit(0)

    def __api_request(self, method, endpoint, data): #data={}):

        response = None

        try:

            #kwargs = dict(data=data)

            response = self.session.request(method, url = endpoint, headers = self.headers, json=data) #**kwargs)

        except Exception as e:

            raise DadesObertesNetworkError(f"Could not complete request: {e}")

        if response is None:

            raise DadesObertesIllegalArgumentError("Illegal request.")

        if not response.ok:

                try:
                    if isinstance(response, dict) and 'error' in response:
                        error_msg = response['error']
                    elif isinstance(response, str):
                        error_msg = response
                    else:
                        error_msg = None
                except ValueError:
                    error_msg = None

                if response.status_code == 404:
                    ex_type = DadesObertesNotFoundError
                    if not error_msg:
                        error_msg = 'Endpoint not found.'
                        # this is for compatibility with older versions
                        # which raised DadesObertesAPIError('Endpoint not found.')
                        # on any 404
                elif response.status_code == 400:
                    return response
                elif response.status_code == 401:
                    ex_type = DadesObertesUnauthorizedError
                elif response.status_code == 422:
                    return response
                elif response.status_code == 500:
                    ex_type = DadesObertesInternalServerError
                elif response.status_code == 502:
                    ex_type = DadesObertesBadGatewayError
                elif response.status_code == 503:
                    #ex_type = DadesObertesServiceUnavailableError
                    return response
                elif response.status_code == 504:
                    ex_type = DadesObertesGatewayTimeoutError
                elif response.status_code >= 500 and \
                    response.status_code <= 511:
                    ex_type = DadesObertesServerError
                else:
                    ex_type = DadesObertesAPIError

                raise ex_type(
                    'DadesObertes API returned error',
                    response.status_code,
                    response.reason,
                    error_msg)

                return response

        else:

            return response

    @staticmethod
    def __json_allow_dict_attrs(json_object):
        """
        Makes it possible to use attribute notation to access a dicts
        elements, while still allowing the dict to act as a dict.
        """
        if isinstance(json_object, dict):
            return AttribAccessDict(json_object)
        return json_object

##
# Exceptions
##
class DadesObertesError(Exception):
    """Base class for DadesObertes.py exceptions"""

class DadesObertesIOError(IOError, DadesObertesError):
    """Base class for DadesObertes.py I/O errors"""

class DadesObertesNetworkError(DadesObertesIOError):
    """Raised when network communication with the server fails"""
    pass
class DadesObertesAPIError(DadesObertesError):
    """Raised when the forgejo API generates a response that cannot be handled"""
    pass
class DadesObertesServerError(DadesObertesAPIError):
    """Raised if the Server is malconfigured and returns a 5xx error code"""
    pass
class DadesObertesInternalServerError(DadesObertesServerError):
    """Raised if the Server returns a 500 error"""
    pass

class DadesObertesBadGatewayError(DadesObertesServerError):
    """Raised if the Server returns a 502 error"""
    pass

class DadesObertesServiceUnavailableError(DadesObertesServerError):
    """Raised if the Server returns a 503 error"""
    pass
class DadesObertesGatewayTimeoutError(DadesObertesServerError):
    """Raised if the Server returns a 504 error"""
    pass
class DadesObertesNotFoundError(DadesObertesAPIError):
    """Raised when the forgejo API returns a 404 Not Found error"""
    pass

class DadesObertesUnauthorizedError(DadesObertesAPIError):
    """Raised when the forgejo API returns a 401 Unauthorized error

       This happens when an OAuth token is invalid or has been revoked,
       or when trying to access an endpoint that can't be used without
       authentication without providing credentials."""
    pass
