# Rodalies
Estat de les línies de Rodalies a Catalunya, publicat a [mastodont.cat](https://mastodont.cat/@rodalies), segons la informació oficial que dona la Generalitat via RSS a [http://www.gencat.cat/rodalies/incidencies_rodalies_rss_{línia}_ca_ES.xml](http://www.gencat.cat/rodalies/incidencies_rodalies_rss_{línia}_ca_ES.xml)

### Dependències

-   **Python 3**
-   Compte del bot a Mastodon
-   Servidor Postgresql

### Com instal·lar Rodalies:

1. Clona el repositori: `git clone https://codeberg.org/spla/rodalies.git <directori>`  

2. Entra amb `cd` al directori `<directori>` i crea l'Entorn Virtual de Python: `python3.x -m venv .`  
 
3. Activa l'Entorn Virtual de Python: `source bin/activate`  
  
4. Executa `pip install -r requirements.txt` per a afegir les llibreries necessàries.  

5. Executa `python rodalies.py` per a aconseguir els tokens d'accés del compte del bot de  Mastodon i crear la base de dades.  

6. Fes servir el teu programador de tasques preferit (per exemple crontab) per a que `python rodalies.py` s'executi cada minut. El bot a Mastodon accepta peticions de qualsevol usuari del fedivers en el format (per exemple):  

@bot r1   

7. Fes servir el teu programador de tasques preferit (per exemple crontab) per a que `python incidencies.py` s'executi cada hora. El bot publicarà a Mastodon el estat de totes les línies a les 7:00 i 16:00 hores i canvis d'estat de qualsevol línia cada hora.  

8.1.2024 *Novetat* Resposta en temps real gràcies a la Streaming API de Mastodon




