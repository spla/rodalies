from app.libraries.setup import Setup
from app.libraries.database import Database
from mastodon import Mastodon
from datetime import datetime, date
import re
import time
import sys
import os
import requests
from io import BytesIO
import feedparser
import emojis
import pdb

linies = ['r1','r2','r3','r4','r7','r8','r11','r12','r13','r14','r15','r16']

def remove_tz(published):
    print(time.localtime().tm_isdst)
    return published.replace(" +0100","") if time.localtime().tm_isdst == 0 else published.replace(" +0200","")

def check_line(line):

    print(f'Verificant {line}')

    line_status = ''

    try:

        resp = requests.get(f'http://www.gencat.cat/rodalies/incidencies_rodalies_rss_{line}_ca_ES.xml', timeout=3)

    except requests.exceptions.ConnectTimeout as conntimeout:

        pass

    if resp.ok:

        content = BytesIO(resp.content)

        feed = feedparser.parse(content)

        title = feed.feed.title

        line_status = feed.entries[0].title

        published = feed.feed.published

        published = datetime.strptime(remove_tz(published), '%a, %d %b %Y %H:%M:%S')

        published = published.strftime('%d %b %Y %H:%M:%S')

    return resp, line_status, published

if __name__ == '__main__':

    setup = Setup()

    db = Database()

    mastodon = Mastodon(
            access_token = setup.mastodon_app_token,
            api_base_url= setup.mastodon_hostname
            )

    date = datetime.now()

    if date.hour == 7 or date.hour == 16:

        post_text = f'Estat del servei de rodalies ({emojis.encode(":x:")} = incidència)\n\n'

        for linia in list(dict.fromkeys(linies)):

            resp, line_status, published = check_line(linia)

            if resp.ok:

                if 'Normalitat al servei' in line_status:

                    post_text += f'{linia.upper()} {emojis.encode(":white_check_mark:")} ({published})\n'

                    status = True

                else:

                    post_text += f'{linia.upper()} {emojis.encode(":x:")} ({published})\n'

                    status = False

                db.save(date, linia, status, line_status)

        if resp.ok:

            print(f'{post_text}')
            mastodon.status_post(post_text)

    else:

        post_text = "Canvis d'estat\n\n"

        lines_read = db.read()

        for line in lines_read:

            change = False

            resp, line_status, published = check_line(line[1]) # verifica estat actual

            if resp.ok:

                if line[2] == False and 'Normalitat al servei' in line_status: #si l'estat anterior era d'incidència però ara ja no

                    post_text += f'{line[3]}\n{line[1]} {emojis.encode(":white_check_mark:")}\n\n'

                    status = True

                    change = True

                elif line[2] == True and not 'Normalitat al servei' in line_status: #si l'estat anterior era OK però ara hi ha incidència

                    post_text += f'Incidència en la línia {line[1]}\n{line_status}\n{line[1]} {emojis.encode(":x:")}\n\n'

                    status = False

                    change = True

                else:

                    status = line[2]

                db.save(date, line[1], status, line_status)

        if change:

            print(f'{post_text}')

            mastodon.status_post(post_text)
